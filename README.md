# shp2geojson.js
Convert shapefile to geoJSON via a web browser without Server-Side code. This conversion will unzip your file and reproject the data with correct encoding in JavaScript.
